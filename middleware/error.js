const looger = require('../utils/logger');

module.exports = function (err, req, res, next) {
    looger.error(err.message, err);
    res.status(500).send('Problem with the server, please try again later.');
}