const db = require("../modules/db");

class Log {
    constructor(log) {
        this.tagId = log.tagId;
        this.value = log.value;
        this.time = log.time;
    }
}

Log.create = async (log) => {
    let addLog = new Log(log);
    const connection = await db.connection();
    const query = `INSERT INTO logs 
                    (tagId, 
                     value,
                     time) 
                 VALUES (?, ?, ?);`;

    const values = [
        addLog.tagId,
        addLog.value,
        addLog.time,
    ];

    let result = await connection.query(query, values);
    await connection.release();
    return result;
}

module.exports = Log;