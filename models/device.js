const db = require("../modules/db");

class Device {
    constructor(device) {
        this.deviceId = +device.deviceId;
        this.deviceName = device.deviceName;
        this.ip = device.ip;
        this.port = device.port;
        this.timeSearch = device.timeSearch;
        this.timeOut = device.timeOut;
        this.protocol = device.protocol;
        this.maxRegisterToRead = device.maxRegisterToRead;
        this.enable = device.enable;
        this.status = device.status;
    }
}

Device.getDevices = async () => {
    const connection = await db.connection();
    const query = `SELECT *
                   FROM devices
                   ORDER BY INET_ATON(ip);`;
    const devices = await connection.query(query);
    await connection.release();
    return devices;
}

Device.getDeviceByDeviceId = async (deviceId) => {
    const connection = await db.connection();
    const query = `SELECT *  
                   FROM devices 
                   WHERE deviceId = ?;`;
    const devices = await connection.query(query, deviceId);
    await connection.release();
    return devices[0];
}

module.exports = Device;