const Joi = require('joi');
const db = require("../modules/db");

class Tag {
    constructor(tag) {
        this.tagId = tag.tagId;
        this.tagName = tag.tagName;
        this.deviceId = tag.deviceId;
        this.objectType = tag.objectType;
        this.id = tag.id;
        this.indexArray = tag.indexArray;
        this.bitOffset = tag.bitOffset;
        this.calculator = tag.calculator;
        this.timeSearch = tag.timeSearch;
        this.minValueToWrite = tag.minValueToWrite;
        this.maxValueToWrite = tag.maxValueToWrite;
        this.enableWriting = tag.enableWriting;
        this.address = tag.address;
        this.valueToWrite = tag.valueToWrite ;
    }
}

Tag.validateTag = (tag) => {

    const schema = Joi.object({
        tagId: Joi.number().optional().allow(null),
        deviceName: Joi.string().optional().allow(null),
        tagName: Joi.string().max(256).required(),
        deviceId: Joi.number().required(),
        objectType: Joi.number().min(1).max(4).required(),
        id: Joi.number().required(),
        indexArray: Joi.number().min(1).required(),
        bitOffset: Joi.number().required(),
        calculator: Joi.string().max(45).required(),
        timeSearch: Joi.number().required(),
        minValueToWrite: Joi.number().required(),
        maxValueToWrite: Joi.number().required(),
        enableWriting: Joi.boolean().truthy(1).falsy(0).required(),
        address: Joi.number().optional().allow(null),
        valueToWrite: Joi.number().min(0).max(65535).required(),
    });

    return schema.validate(tag);
}

Tag.getTags = async () => {
    const connection = await db.connection();
    const query = `SELECT *
                   FROM tags
                   ORDER BY id, indexArray, bitOffset, objectType;`;
    const tags = await connection.query(query);
    await connection.release();
    return tags;
}

Tag.getTagByTagId = async (tagId) => {
    const connection = await db.connection();
    const query = `SELECT *  
                   FROM tags 
                   WHERE tagId = ?;`;
    const tag = await connection.query(query, tagId);
    await connection.release();
    return tag[0];
}

Tag.getTagsByDeviceId = async (deviceId) => {
    const connection = await db.connection();
    const query = `SELECT *  
                   FROM tags 
                   WHERE deviceId = ?;`;
    const tags = await connection.query(query, deviceId);
    await connection.release();
    return tags;
}

Tag.getTagsWithAddresses = async () => {
    const connection = await db.connection();
    const query = `SELECT tags.tagId, tags.address  
                   FROM tags 
                   WHERE address IS NOT NULL;`;
    const tags = await connection.query(query);
    await connection.release();
    return tags;
}

module.exports = Tag;
