class Area {
    constructor(tags, objectType, id, unitDisconnected, minIndexArray, maxIndexArray, amountToRead) {
        this.tags = tags;
        this.objectType = objectType;
        this.id = id;
        this.unitDisconnected = unitDisconnected;
        this.minIndexArray = minIndexArray;
        this.maxIndexArray = maxIndexArray;
        this.amountToRead = amountToRead;
    }
}

module.exports = Area;