const Parser = require('expr-eval').Parser;

const functions = {
    convertArrayToObject: (array, key) => {
        const initialValue = {};
        return array.reduce((obj, item) => {
            return {
                ...obj,
                [item[key]]: item,
            };
        }, initialValue);
    },

    sortBy: (array, property) => {
        return array.sort((a, b) => (a[property] > b[property]) ? 1 : ((b[property] > a[property]) ? -1 : 0));
    },

    groupBy: (array, property) => {
        var grouped = {};
        for (const key in array) {
            var p = array[key][property];
            if (!grouped[p]) {
                grouped[p] = [];
            }
            grouped[p].push(array[key]);
        }

        return grouped;
    },

    distinctBy: (array, property) => {
        let newArray = [];
        const map = new Map();

        for (const item of array) {
            if (!map.has(item[property])) {
                map.set(item[property], true);
                newArray.push(item);
            }
        }

        return newArray;
    },

    sleep: async (timeout) => {
        await new Promise(resolve => setTimeout(resolve, timeout));
    },

    evaluateCalculation: (calculator, value) => {
        let expression = Parser.parse(calculator.toLowerCase());
        let result = expression.evaluate({
            x: value
        });

        return result;
    },
    evaluateReversedCalculation: (calculator, value) => {
        let newCalculator = calculator.toLowerCase();
        if (newCalculator === 'x')
            return value;

        if (newCalculator.includes("/"))
            newCalculator = newCalculator.replace("/", "*");
        else if (newCalculator.includes("*"))
            newCalculator = newCalculator.replace("*", "/");

        let reversedValue = functions.evaluateCalculation(newCalculator, value);
        return reversedValue;
    },
    modifyBit: (n, p, b) => {
        let mask = 1 << p;
        return (n & ~mask) | ((b << p) & mask);
    }
}

module.exports = functions;