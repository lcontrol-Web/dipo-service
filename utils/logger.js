const {
    createLogger,
    transports,
    format
} = require('winston');

const logger = createLogger({
    transports: [
        new transports.File({
            filename: 'logs/info.log',
            level: 'info',
            maxsize: 10000000,
            maxFiles: 50,
            format: format.combine(format.timestamp(), format.json())
        }),
        new transports.File({
            filename: 'logs/error.log',
            level: 'error',
            maxsize: 10000000,
            maxFiles: 50,
            handleRejections: true,
            handleExceptions: true,
            format: format.combine(format.timestamp(), format.json())
        }),
        new transports.Console({
            level: 'error',
            handleRejections: true,
            handleExceptions: true,
            format: format.prettyPrint(),
        })
    ]
})

if (process.env.NODE_ENV !== 'production') {
    logger.add(new transports.Console({
        format: format.simple()
    }))
}

module.exports = logger;