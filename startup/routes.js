const tags = require('../routes/tags');
const devices = require('../routes/devices');
const error = require('../middleware/error');

module.exports = function (app) {
    app.use('/api/tags', tags);
    app.use('/api/devices', devices);
    app.use(error);
};