const express = require('express');
const logger = require("../utils/logger");
const modbus = require('../modules/modbus');
const router = express.Router();

router.get('/status', [], async (req, res) => {
    const devices = modbus.getLocalDevices();
    let devicesStatus = [];
    for (const deviceId in devices) {
        devicesStatus.push({
            deviceId: devices[deviceId].deviceId,
            status: devices[deviceId].status,
        });
    }
    res.send(devicesStatus);
});

router.get('/refresh', [], async (req, res) => {
    modbus.refreshAll();
    logger.info("Service has been refreshed manually");
    res.send({
        success: true
    });
});


router.get('/add-device/:id', [], async (req, res) => {

    if (!req.params.id)
        return res.status(400).send('device ID is required');

    await modbus.addDevice(+req.params.id);
    logger.info("Device added");
    res.send({
        success: true
    });
});

router.get('/update-device/:id', [], async (req, res) => {

    if (!req.params.id)
        return res.status(400).send('device ID is required');

    await modbus.updateDevice(+req.params.id);
    logger.info("Device updated");
    res.send({
        success: true
    });
});

module.exports = router;