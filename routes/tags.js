const express = require('express');
const modbus = require('../modules/modbus');
const Tag = require('../models/tag');
const router = express.Router();

router.post('/values', [], async (req, res) => {
    const devices = modbus.getLocalDevices();
    let tags = req.body;

    for (let tag of tags) {
        let deviceIndex = devices.findIndex(device => device.deviceId == tag.deviceId);
        if (deviceIndex != -1) {
            if (devices[deviceIndex].tags) {
                let tagIndex = devices[deviceIndex].tags.findIndex(temp => temp.tagId == tag.tagId);
                if (tagIndex != -1) {
                    tag.value = devices[deviceIndex].tags[tagIndex].value;
                    tag.calculatedValue = devices[deviceIndex].tags[tagIndex].calculatedValue;
                }
            }
        }
    }

    res.send(tags);
});

router.post('/write', [], async (req, res) => {

    const {
        error
    } = Tag.validateTag(req.body);
    if (error)
        return res.status(400).send(error.details[0].message);

    let value = await modbus.writeToTag(req.body);
    res.send(value.toString());
});

module.exports = router;