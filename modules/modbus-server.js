var ModbusRTU = require("modbus-serial");
var modbus = require("./modbus");
var Log = require("../models/log");
const logger = require('../utils/logger');

var minAddress = 0;
var maxAddress = 100000;
// var coils = Buffer.alloc(160008, 0); // coils and discrete inputs
// var regsiters = Buffer.alloc(160008, 0); // input and holding registers

//     1...10000*  address - 1      Coils/Digital (outputs)   0   Read/Write 
// 10001...20000*  address - 10001  Discrete/Digital Inputs    01  Read
// 30001...40000*  address - 30001  Input/Analog Registers    04  Read
// 40001...50000*  address - 40001  Holding/Analog Output Registers  03  Read/Write

var vector = {
    getCoil: function (addr, unitID) {
        let value = getValue(addr + 1, 0);
        return value;
    },
    getDiscreteInput: function (addr, unitID) {
        let value = getValue(addr + 1, 10000);
        return value;
    },
    getInputRegister: function (addr, unitID) {
        let value = getValue(addr + 1, 30000);
        return value;
    },
    getHoldingRgetMultipleHoldingRegistersegister: function (addr, unitID) {
        let value = getValue(addr + 1, 40000);
        return value;
    },
    getMultipleHoldingRegisters: function (addr, length, unitID) {
        let values = getValues(addr + 1, length, 40000);
        return values;
    },
    getMultipleInputRegisters: function (addr, length, unitID) {
        let values = getValues(addr + 1, length, 30000);
        return values;
    },
    setCoil: function (addr, value, unitID) {
        setValueAndWriteToLog(addr + 1, value, 0);
    },
    setRegister: function (addr, value, unitID) {
        setValueAndWriteToLog(addr + 1, value, 40000);
    }
};

function getValue(addr, subtract) {
    try {
        if (addr >= 0 && addr < maxAddress) {
            let tag = findTagByAddress(addr, subtract);
            if (!tag || !tag.hasOwnProperty('value'))
                return 0;
            else
                return tag.value;
        }
    } catch (error) {
        logger.error(error.stack);
    }
}

function getValues(addr, length, subtract) {
    try {
        if (addr >= 0 && addr < maxAddress) {
            let values = new Array(length).fill(0);
            for (let i = 0; i < length; i++) {
                let tag = findTagByAddress(addr + i, subtract);
                if (tag && tag.hasOwnProperty('value'))
                    values[i] = tag.value;
            }
            return values;
        }
    } catch (error) {
        logger.error(error.stack);
    }
}

async function setValueAndWriteToLog(addr, value, subtract) {
    try {
        if (addr <= minAddress && addr > maxAddress || value <= 0 && value >= 65535)
            return new Error('cannot write to tag');

        let tag = findTagByAddress(addr, subtract);
        if (tag) {
            tag.valueToWrite = value;
            let returnedValue = await modbus.writeToTag(tag);
            await Log.create({
                tagId: tag.tagId,
                value: value,
                time: new Date()
            });
            return returnedValue;
        }
    } catch (error) {
        logger.error(error.stack);
    }
}

function findTagByAddress(address, subtract) {
    let devices = modbus.getLocalDevices();
    for (const device of devices) {
        let index = device.tags.findIndex(tag => (tag.address - subtract) == address);
        if (index != -1)
            return device.tags[index];
    }

    return null;
}

var serverTCP = new ModbusRTU.ServerTCP(vector, {
    host: process.env.MODBUS_SERVER_HOST,
    port: process.env.MODBUS_SERVER_PORT,
    debug: true,
    unitID: 1
});

serverTCP.on("initialized", function () {
    logger.info(`ModbusTCP listening on modbus://${ process.env.MODBUS_SERVER_HOST}:${process.env.MODBUS_SERVER_PORT}`);
});

serverTCP.on("socketError", function (err) {
    logger.error(err);
    serverTCP.close(closed);
});

serverTCP.on("error", function (err) {
    logger.error(err);
});

function closed() {
    logger.info("server closed");
}