const ModbusRTU = require("modbus-serial");
const Device = require("../models/device");
const Tag = require("../models/tag");
const Area = require("../models/area");
const logger = require("../utils/logger");
const objectTypes = require("../enums/object-types");
const utilsFunctions = require("../utils/functions");

var devices = [];
var clients = [];
// var addresses = {};

async function start() {
    devices = await Device.getDevices();
    // let addressesArray = await Tag.getTagsWithAddresses();
    // addresses = utilsFunctions.convertArrayToObject(addressesArray, 'address');

    for (let device of devices) {
        device.tags = await Tag.getTagsByDeviceId(device.deviceId);
        connect(device);
    }
}

async function connect(device) {

    try {
        if (device.enable) {
            device.status = false;
            let client = new ModbusRTU();
            client.deviceId = device.deviceId;
            await client.setTimeout(2000);
            await client.connectTCP(device.ip, {
                port: device.port
            });

            device.status = true;
            clients.push(client);
            logger.info(`Connected to ${device.deviceName} | ${device.ip}:${device.port}`);
            logger.info(`Total clients: ${clients.length}`);

            device.areas = getAreas(device.tags, device.maxRegisterToRead);
            readAreas(client, device);
        }
    } catch (error) {
        logger.info(`Connection failed to ${device.deviceName} | ${device.ip}:${device.port}`);
        setTimeout(() => {
            device.status = false;
            connect(device);
        }, 10000);
    }
}

function getAreas(tags, maxRegisterToRead) {
    let areas = [];
    let groupedTagsByObjectType = utilsFunctions.groupBy(tags, 'objectType');

    for (const objectType in groupedTagsByObjectType) {
        let groupedTagsById = utilsFunctions.groupBy(groupedTagsByObjectType[objectType], 'id');

        for (const id in groupedTagsById) {
            let sortedTagsByIndexArray = utilsFunctions.sortBy(groupedTagsById[+id], 'indexArray');
            let distinctedTagsByIndexArray = utilsFunctions.distinctBy(sortedTagsByIndexArray, 'indexArray');
            let splitedTags = splitByMaxRegisterToReadAndBreakSequence(distinctedTagsByIndexArray, maxRegisterToRead);

            for (const tags of splitedTags) {
                let area = new Area();
                area.tags = tags;
                area.id = +id;
                area.objectType = +objectType;
                area.minIndexArray = tags[0].indexArray;
                area.maxIndexArray = tags[tags.length - 1].indexArray;
                area.amountToRead = tags.length;
                areas.push(area);
            }
        }
    }

    return areas;
}

async function readAreas(client, device) {
    try {
        // if we not have any tags so check the connection
        if (!client.isOpen)
            await disconnect(device);
        else {
            for (const area of device.areas) {
                if (device.status) {
                    let result = await readArea(area, client, device);
                    if (result != -1)
                        analyzeResult(result, area, device);
                    await utilsFunctions.sleep(200);
                }
            }
        }
    } catch (error) {
        logger.error(error);
    } finally {
        // execute the function as soon as possibale after reading all the areas
        setImmediate(() => {
            if (device.status)
                readAreas(client, device);
        });
    }
}

async function readArea(area, client, device) {
    try {
        await client.setID(area.id);
        const clientId = await client.getID();
        if (clientId == area.id) {
            let result;
            switch (area.objectType) {
                case objectTypes.AnalogInput: {
                    result = await client.readInputRegisters(area.minIndexArray - 1, area.amountToRead);
                    break;
                }
                case objectTypes.AnalogOutput: {
                    result = await client.readHoldingRegisters(area.minIndexArray - 1, area.amountToRead);
                    break;
                }
                case objectTypes.DigitalInput: {
                    result = await client.readDiscreteInputs(area.minIndexArray - 1, area.amountToRead);
                    break;
                }
                case objectTypes.DigitalOutput: {
                    result = await client.readCoils(area.minIndexArray - 1, area.amountToRead);
                    break;
                }
            }

            return result;
        }
    } catch (err) {
        await readingErrorOccured(err, area, device);
        return -1;
    }
}

function analyzeResult(result, area, device) {

    let offset = 0;
    let value;

    for (const tag of area.tags) {
        switch (area.objectType) {
            case objectTypes.AnalogInput:
            case objectTypes.AnalogOutput: {
                value = result.buffer.readUInt16BE(offset);

                if (tag.bitOffset == 0) {
                    setValueToTag(tag, value);
                } else {
                    let bitOffsetTags = getBitOffsetTags(device.tags, area, tag);
                    if (bitOffsetTags.length > 0) {
                        for (const bitOffsetTag of bitOffsetTags) {
                            let newValue = (value >> bitOffsetTag.bitOffset - 1) & 1;
                            setValueToTag(bitOffsetTag, newValue);
                        }
                    }
                }

                offset += 2;
                break;
            }
            case objectTypes.DigitalInput:
            case objectTypes.DigitalOutput: {
                let start = tag.indexArray - area.tags[0].indexArray;
                value = result.data.slice(start, start + 1)[0] ? 1 : 0;
                setValueToTag(tag, value);
                break;
            }
            default:
                break;
        }
    }

    return value;
}

function setValueToTag(tag, value) {
    tag.value = value;
    tag.calculatedValue = utilsFunctions.evaluateCalculation(tag.calculator, value);
}

async function writeToTag(temTag) {
    const tag = new Tag(temTag);
    const client = clients.find(client => client.deviceId == tag.deviceId);
    const device = devices.find(device => device.deviceId == tag.deviceId);

    try {
        if (client && device && client.isOpen && device.status) {
            await client.setID(tag.id);
            const clientId = await client.getID();
            if (clientId == tag.id) {
                switch (tag.objectType) {
                    case objectTypes.AnalogOutput: {

                        let value;

                        if (tag.bitOffset == 0)
                            value = utilsFunctions.evaluateReversedCalculation(tag.calculator, tag.valueToWrite);
                        else if (tag.bitOffset > 0) {
                            let result = await client.readHoldingRegisters(tag.indexArray - 1, 1);
                            value = utilsFunctions.modifyBit(result.data[0], tag.bitOffset - 1, tag.valueToWrite);
                        }

                        await client.writeRegister(tag.indexArray - 1, value);
                        break;
                    }
                    case objectTypes.DigitalOutput: {
                        await client.writeCoil(tag.indexArray - 1, tag.valueToWrite);
                        break;
                    }
                    default:
                        break;
                }

                for (const area of device.areas) {
                    for (const tagArea of area.tags) {
                        if (tagArea.tagId == tag.tagId) {
                            let result = await readArea(area, client, device);
                            if (result != -1) {
                                let value = analyzeResult(result, area, device);
                                return value;
                            } else
                                return null;
                        }
                    }
                }
            }
        }
    } catch (error) {
        writingErrorOccured(error, tag, device);
    }
}

async function readingErrorOccured(err, area, device) {
    logger.error(err.message);

    // unit disconnected
    if (err.modbusCode === 11) {
        logger.error(`Unit ID  ${area.id} disconnected  ${device.deviceName} ${device.ip}:${device.port}`, err.message);
    }
    // device not connected or unknown error 
    else if (err.errno === "ETIMEDOUT" || err.errno === "ECONNREFUSED") {
        logger.error(`Disconnecting from ${device.deviceName} ${device.ip}:${device.port}`, err.message);
        if (device.status) {
            await disconnect(device);
            connect(device);
        }
    } else {
        logger.error(`Cannot read from ${device.deviceName} | ${device.ip}:${device.port} |
            ID: ${area.id} | 
            Object type: ${getObjectTypeName(area.objectType)} |
            Min index array: ${area.minIndexArray} | 
            Max index array: ${area.maxIndexArray} | 
            Amount : ${area.amountToRead}`);
    }
}

function writingErrorOccured(err, tag, device) {
    logger.error(`Cannot write to ${device.deviceName} | tag name: ${tag.tagName} | tag ID: ${tag.id} | index array: ${tag.indexArray}`);
}

function getBitOffsetTags(tags, area, tag) {
    let bitOffsetTags = [];

    for (const tempTag of tags) {
        if (tempTag.deviceId == tag.deviceId &&
            area.objectType == tempTag.objectType &&
            area.id == tempTag.id &&
            tag.indexArray == tempTag.indexArray &&
            tempTag.bitOffset > 0)
            bitOffsetTags.push(tempTag);
    }

    return bitOffsetTags;
}

function splitByMaxRegisterToReadAndBreakSequence(tags, maxRegistersToRead) {
    const result = tags.reduce((r, tag) => {
            const lastSubArray = r[r.length - 1];

            if (lastSubArray) {
                // check the sequence of the index array 
                let currentIndexArray = tag.indexArray;
                let lastIndexArray = lastSubArray[lastSubArray.length - 1].indexArray;

                if ((currentIndexArray - lastIndexArray) > 1 || lastSubArray.length >= maxRegistersToRead)
                    r.push([]);

            } else if (!lastSubArray)
                r.push([]);

            r[r.length - 1].push(tag);
            return r;
        },
        []);

    return result;
}

async function disconnect(device) {
    device.status = false;
    device.areas = [];
    await utilsFunctions.sleep(200);
    let index = clients.findIndex((client) => client.deviceId == device.deviceId);
    if (index != -1) {
        clients[index].close();
        clients.splice(index, 1);
        logger.info(`Total clients: ${clients.length}`);
    }
}

async function addDevice(deviceId) {
    let device = await Device.getDeviceByDeviceId(deviceId);
    device.tags = await Tag.getTagsByDeviceId(deviceId);
    devices.push(device);
    connect(device);
}

async function updateDevice(deviceId) {
    let index = devices.findIndex(tempDevice => tempDevice.deviceId == deviceId);
    if (index != -1) {
        // !important 
        // send the device by reference
        await disconnect(devices[index]);
        devices[index].enable = false;
        devices.splice(index, 1);
        await addDevice(deviceId);
    }
}

async function refreshAll() {

    for (let device of devices) {
        await disconnect(device);
        device.enable = false;
    }

    devices = [];
    clients = [];
    start();
}

function getLocalDevices() {
    return devices;
}

function getObjectTypeName(objectType) {
    switch (objectType) {
        case 1:
            return 'Analog Input';
        case 2:
            return 'Analog Output';
        case 3:
            return 'Digital Input';
        case 4:
            return 'Digital Output';
        default:
            break;
    }
}

module.exports.start = start;
module.exports.writeToTag = writeToTag;
module.exports.getLocalDevices = getLocalDevices;
module.exports.addDevice = addDevice;
module.exports.updateDevice = updateDevice;
module.exports.refreshAll = refreshAll;