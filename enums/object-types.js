const objectTypes = Object.freeze({
    AnalogInput: 1,
    AnalogOutput: 2,
    DigitalInput: 3,
    DigitalOutput: 4
});

module.exports = objectTypes;