const express = require('express');
require('express-async-errors');
const app = express();
const cors = require('cors');
const logger = require('./utils/logger');
require('dotenv').config({
    path: __dirname + '/.env'
});

app.use(express.json({
    limit: '50mb'
}));

app.use(cors());

const modbus = require('./modules/modbus');
require('./startup/routes')(app);
require('./modules/modbus-server');

const port = process.env.PORT;
const server = app.listen(port, async () => {
    logger.info(`API Server listening on http://localhost:${port}`);
    modbus.start();
});

module.exports = server;